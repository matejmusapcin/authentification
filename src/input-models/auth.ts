import { body } from 'express-validator';

const registerUserSchema = [
    body('username', 'Check the form of the name input')
        .exists({ checkFalsy: true })
        .isString()
        .notEmpty(),
    body('email', 'Check the form of the email input')
        .exists({ checkFalsy: true })
        .isEmail()
        .notEmpty(),
    body('password', 'Chech the form of the password input')
        .exists({ checkFalsy: true })
        .isString()
        .notEmpty()
];

const loginUserSchema = [
    body('email', 'Email is not valid').isEmail(),
    body('password', 'Password is not valid').isString()
];

const forgotPasswordSchema = [
    body('email', 'Email is not valid').isEmail(),
    body('password', 'Password is not valid').isString()
];

const resetPasswordSchema = [
    body('email', 'Email is not valid').isEmail(),
    body('password', 'Password is not valid').isString()
];

const appleLoginSchema = [
    body('email', 'Email is not valid').isEmail(),
    body('password', 'Password is not valid').isString()
];

const googleLoginSchema = [
    body('email', 'Email is not valid').isEmail(),
    body('password', 'Password is not valid').isString()
];

const facebookLoginSchema = [
    body('email', 'Email is not valid').isEmail(),
    body('password', 'Password is not valid').isString()
];

export {
    registerUserSchema,
    loginUserSchema,
    forgotPasswordSchema,
    resetPasswordSchema,
    appleLoginSchema,
    googleLoginSchema,
    facebookLoginSchema
};
