'use strict';

import express from 'express';
import swaggerUi from 'swagger-ui-express';
import { generateDocumentation } from '../swagger';
import auth from './auth';

const router = express.Router();

router.use('/', swaggerUi.serve);
router.use('/auth', auth);

const documentation = generateDocumentation();

router.get('/api-docs', swaggerUi.setup(documentation));
router.get('/swagger', (_req: any, res: any) => {
    return res.send(JSON.stringify(documentation, null, 2));
});

export default router;
