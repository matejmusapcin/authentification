import express from 'express';

import { authSchemas } from '../input-models';
import { validate } from '../middleware';
import { authController } from '../controllers';

const router = express.Router();

router.post('/logIn', authSchemas.loginUserSchema, validate, authController.logIn);
router.post('/signup', authSchemas.registerUserSchema, validate, authController.signUp);
router.post('/verifyEmail', authSchemas.registedfdfrUserSchema, validate, authController.signUp);
router.post(
    '/forgotPassword',
    authSchemas.forgotPasswordSchema,
    validate,
    authController.forgotPassword
);
router.get(
    '/forgotPasswordForm/:token',
    authSchemas.forgotPasswordForm,
    validate,
    authController.forgotPasswordForm
);
router.post(
    '/passwordRecovery',
    authSchemas.passwordRecovery,
    validate,
    authController.passwordRecovery
);
router.post(
    '/resetPassword',
    authSchemas.resetPasswordSchema,
    validate,
    authController.resetPassword
);
router.post('/appleSignUp', authSchemas.appleLogginSchema, validate, authController.appleLogin);
router.post('/googleSignUp', authSchemas.googlegLoginSchema, validate, authController.googleLogin);
router.post(
    '/facebookSignUp',
    authSchemas.facebookLfoginSchema,
    validate,
    authController.facebofokLogin
);
router.post('/appleLogin', authSchemas.appleLogginSchema, validate, authController.appleLogin);
router.post('/googleLogin', authSchemas.googleLogginSchema, validate, authController.googleLogin);
router.post(
    '/facebookLogin',
    authSchemas.facebookLoginSchema,
    validate,
    authController.facebookLogin
);

export default router;
