import { NextFunction, Request, Response } from 'express';
import { hashPassword } from '../util';

  signIn: async (req, res, next) => {
    try {
      const { email, password } = res.locals.input;

      const user = await userQueries.getUserByEmail(email);
      if (!user) {
        return res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.INVALID_EMAIL,
          message: responseMessages.WRONG_EMAIL
        });
      }
      user.oauth = user.oauth ? true : false;
      user.notifications = user.notifications ? true : false;
      user.smsNotifications = user.smsNotifications ? true : false;

      if (user.oauth) {
        return res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.INVALID_EMAIL,
          message: responseMessages.DIFFERENT_LOGIN_METHOD
        });
      }

      comparePassword(password, user.password)
        .then(async isCorrect => {
          if (!isCorrect) {
            return res.status(statusCodes.UNAUTHORIZED).send({
              data: {},
              code: responseCodes.INVALID_PASSWORD,
              message: responseMessages.INVALID_PASSWORD
            });
          }

          if (user.status == 'unconfirmed') {
            return res.status(statusCodes.UNAUTHORIZED).send({
              data: {},
              code: responseCodes.CONFIRM_EMAIL,
              message: responseMessages.CONFIRM_EMAIL
            });
          }

          if (user.status == 'deleted') {
            const isConfirming = await userQueries.getSecretTokenInUse(user.ID);
            if (isConfirming) {
              return res.status(statusCodes.UNAUTHORIZED).send({
                data: {},
                code: responseCodes.CONFIRM_EMAIL,
                message: responseMessages.CONFIRM_EMAIL
              });
            }

            return res.status(statusCodes.UNAUTHORIZED).send({
              data: {},
              code: responseCodes.USER_DELETED,
              message: responseMessages.USER_DELETED
            });
          }

          delete user.password;
          delete user.tzTimeZone;

          const token = await generateJwtToken(user.ID, user.email);
          res.set('auth-token', token.toString());
          return res.status(statusCodes.OK).send({
            data: {
              user
            },
            code: responseCodes.OK,
            message: responseMessages.OK
          });
        });
    } catch (e) {
      console.log(e);
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },
  dashboardSignIn: async (req, res, next) => {
    try {
      const { email, password } = res.locals.input;

      const userData = await dashboardQueries.getCoachOrAdminByEmail(email);
      if (!userData.role) {
        return res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.INVALID_EMAIL,
          message: responseMessages.WRONG_EMAIL
        });
      }

      if (!userData.user.password) {
        return res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.INVALID_PASSWORD,
          message: responseMessages.INVALID_PASSWORD
        });
      }

      comparePassword(password, userData.user.password)
        .then(async isCorrect => {
          if (!isCorrect) {
            return res.status(statusCodes.UNAUTHORIZED).send({
              data: {},
              code: responseCodes.INVALID_PASSWORD,
              message: responseMessages.INVALID_PASSWORD
            });
          }

          let names = userData.user.fullName.split(" ");
          let user = { ID: userData.user.ID, firstName: names[0], lastName: names.length > 1 ? names[names.length - 1] : "", fullName: userData.user.fullName, email: userData.user.emailAddress, profileIcon: userData.user.profileIcon, oauth: false, notifications: false, partnerId: userData.user.partnerId, role: userData.role };
          if (userData.role == "coach") {
            user.partnerId = userData.user.partnerID;
          }

          const token = await generateJwtToken(user.ID, { role: userData.role });
          user.token = token.toString();

          res.cookie('auth-token', token.toString());
          return res.status(statusCodes.OK).send({
            data: {
              user
            },
            code: responseCodes.OK,
            message: responseMessages.OK
          });

        });
    } catch (e) {
      console.log(e);
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },
  signUp: async (req, res, next) => {
    res.set('auth-token', null);
    const user = res.locals.input;

    try {
      const existingUser = await userQueries.getUserByEmail(user.email);
      if (existingUser) {
        if (existingUser.status != 'deleted') {
          return res.status(statusCodes.NOT_ACCEPTABLE).send({
            data: {},
            code: responseCodes.EMAIL_TAKEN,
            message: responseMessages.EMAIL_TAKEN
          });
        }
      }

      user.hashedPassword = await generatePasswordHash(user.password.toString());
      const secretToken = user.secretToken = uuid.v4().slice(24);

      if (existingUser) {
        user.ID = existingUser.ID;
        user.status = existingUser.status;
        await userQueries.setUserSecretToken(user.ID, secretToken);
      } else {
        user.ID = await userQueries.createUser(user);
      }

      const token = await generateJwtToken(user.ID, { email: user.email, secretToken, user });

      const createdUser = await userQueries.getUserByID(user.ID);
      delete createdUser['password'];

      let mergeVars = {
        header: "Almost done!",
        paragraph: `Hi ${user.firstName}, to finish signing up click the link below to confirm your email address.`,
        link: `${process.env.URL_SCHEME}${process.env.HOST}/auth/token/${token}/?shouldRedirect=true`,
        link_text: "Confirm Email"
      }

      await mailer.sendEMail(user.email, "Stake Something Email Confirmation", mergeVars);

      // const token = await generateJwtToken(createdUser.ID, createdUser.email);
      // res.set('auth-token', token.toString());  
      return res.status(statusCodes.OK).send({
        data: {
          //user: createdUser,
        },
        code: responseCodes.OK,
        message: responseMessages.OK
      });
    } catch (e) {
      await userQueries.deleteUser(user.ID);
      console.log(e);
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },
  googleSignin: async (req, res) => {
    try {
      let { token, timeZone, tzTimeZone } = res.locals.input;

      const payload = await verify(token).catch(console.error);
      if (!payload) {
        res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.INVALID_TOKEN,
          message: responseMessages.INVALID_TOKEN
        });
      }

      const type = 'google';
      const id = payload['sub'];
      let user = await userQueries.getUserByOAuth({ type, id });
      if (!user) {
        return res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.INVALID_EMAIL,
          message: responseMessages.USER_NOT_FOUND
        });
      }

      if (user.status != 'active') {
        return res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.USER_DELETED,
          message: responseMessages.USER_DELETED
        });
      }

      user.oauth = user.oauth ? true : false;
      user.notifications = user.notifications ? true : false;
      user.smsNotifications = user.smsNotifications ? true : false;

      const authToken = await generateJwtToken(user.ID, user.email);

      res.set('auth-token', authToken.toString());
      res.status(statusCodes.OK).send({
        data: {
          user: _.pick(user, ['ID', 'email', 'firstName', 'lastName', 'profileIcon', 'oauth', 'notifications', 'phoneNumber', 'smsNotifications'])
        },
        code: responseCodes.OK,
        message: responseMessages.OK
      });
    } catch (e) {
      console.log(e);
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },
  googleSignup: async (req, res) => {
    try {
      let { token, timeZone, tzTimeZone } = res.locals.input;

      const payload = await verify(token).catch(console.error);
      if (!payload) {
        res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.INVALID_TOKEN,
          message: responseMessages.INVALID_TOKEN
        });
      }

      const id = payload['sub'];
      const firstName = payload['given_name'];
      const lastName = payload['family_name'];
      const email = payload['email'];
      const profileIcon = payload['picture'];
      const type = 'google';

      const existingOauth = await userQueries.getUserByOAuth({ type, id });
      if (existingOauth) {
        if (existingOauth.status != 'deleted') {
          return res.status(statusCodes.NOT_ACCEPTABLE).send({
            data: {},
            code: responseCodes.EMAIL_TAKEN,
            message: responseMessages.OAUTH_TAKEN
          });
        }
      }

      let user = await userQueries.getUserByEmail(email);
      if (user) {
        if (user.status != 'deleted') {
          //await userQueries.setOAuthData({ email, type, id });
          return res.status(statusCodes.NOT_ACCEPTABLE).send({
            data: {},
            code: responseCodes.EMAIL_TAKEN,
            message: responseMessages.DIFFERENT_LOGIN_METHOD
          });
        }

        await userQueries.reactivateOAuthUser({ firstName, lastName, profileIcon, tzTimeZone, ID: user.ID });
        await userQueries.setOAuthData({ email, type, id });
      } else {
        await userQueries.createNewOAuthUser({ firstName, lastName, email, type, profileIcon, tzTimeZone, id });
      }

      user = await userQueries.getUserByEmail(email);
      user.oauth = user.oauth ? true : false;
      user.notifications = user.notifications ? true : false;
      user.smsNotifications = user.smsNotifications ? true : false;
      if (!user) {
        console.log('no user');
        return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
      }

      const authToken = await generateJwtToken(user.ID, user.email);

      res.set('auth-token', authToken.toString());
      res.status(statusCodes.OK).send({
        data: {
          user: _.pick(user, ['ID', 'email', 'firstName', 'lastName', 'profileIcon', 'oauth', 'notifications', 'phoneNumber', 'smsNotifications'])
        },
        code: responseCodes.OK,
        message: responseMessages.OK
      });
    } catch (e) {
      console.log(e);
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },
  facebookSignin: async (req, res) => {
    try {
      let { token, timeZone, tzTimeZone } = res.locals.input;

      const verified = await facebookVerification.verify(token);
      if (!verified || !verified.is_valid) {
        res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.INVALID_TOKEN,
          message: responseMessages.INVALID_TOKEN
        });
      }

      const userInfo = await facebookVerification.getUserInfo(token);

      const type = 'facebook';
      const id = userInfo.id;
      let user = await userQueries.getUserByOAuth({ type, id });
      if (!user) {
        return res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.INVALID_EMAIL,
          message: responseMessages.USER_NOT_FOUND
        });
      }

      if (user.status != 'active') {
        return res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.USER_DELETED,
          message: responseMessages.USER_DELETED
        });
      }
      user.oauth = user.oauth ? true : false;
      user.notifications = user.notifications ? true : false;
      user.smsNotifications = user.smsNotifications ? true : false;

      const authToken = await generateJwtToken(user.ID, user.email);

      res.set('auth-token', authToken.toString());
      res.status(statusCodes.OK).send({
        data: {
          user: _.pick(user, ['ID', 'email', 'firstName', 'lastName', 'profileIcon', 'oauth', 'notifications', 'phoneNumber', 'smsNotifications'])
        },
        code: responseCodes.OK,
        message: responseMessages.OK
      });
    } catch (e) {
      console.log(e);
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },
  facebookSignup: async (req, res) => {
    try {
      let { token, timeZone, tzTimeZone } = res.locals.input;

      const verified = await facebookVerification.verify(token);
      if (!verified || !verified.is_valid) {
        res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.INVALID_TOKEN,
          message: responseMessages.INVALID_TOKEN
        });
      }

      const userInfo = await facebookVerification.getUserInfo(token);
      const type = 'facebook';
      const id = userInfo.id;
      const email = userInfo.email;
      const firstName = userInfo.first_name;
      const lastName = userInfo.last_name;
      const profileIcon = userInfo.picture ? userInfo.picture.data.url : null;

      const existingOauth = await userQueries.getUserByOAuth({ type, id });
      if (existingOauth) {
        if (existingOauth.status != 'deleted') {
          return res.status(statusCodes.NOT_ACCEPTABLE).send({
            data: {},
            code: responseCodes.EMAIL_TAKEN,
            message: responseMessages.OAUTH_TAKEN
          });
        }
      }

      let user = await userQueries.getUserByEmail(email);
      if (user) {
        if (user.status != 'deleted') {
          //await userQueries.setOAuthData({ email, type, id });
          return res.status(statusCodes.NOT_ACCEPTABLE).send({
            data: {},
            code: responseCodes.EMAIL_TAKEN,
            message: responseMessages.DIFFERENT_LOGIN_METHOD
          });
        }

        await userQueries.reactivateOAuthUser({ firstName, lastName, profileIcon, tzTimeZone, ID: user.ID });
        await userQueries.setOAuthData({ email, type, id });
      } else {
        await userQueries.createNewOAuthUser({ firstName, lastName, email, type, profileIcon, tzTimeZone, id });
      }

      user = await userQueries.getUserByEmail(userInfo.email);
      user.oauth = user.oauth ? true : false;
      user.notifications = user.notifications ? true : false;
      user.smsNotifications = user.smsNotifications ? true : false;
      if (!user) {
        console.log('no user');
        return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
      }

      const authToken = await generateJwtToken(user.ID, user.email);

      res.set('auth-token', authToken.toString());
      res.status(statusCodes.OK).send({
        data: {
          user: _.pick(user, ['ID', 'email', 'firstName', 'lastName', 'profileIcon', 'oauth', 'notifications', 'phoneNumber', 'smsNotifications'])
        },
        code: responseCodes.OK,
        message: responseMessages.OK
      });
    } catch (e) {
      console.log(e);
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },
  appleSignin: async (req, res) => {
    try {
      let { token, timeZone, tzTimeZone } = res.locals.input;

      let user = await userQueries.getUserByOAuth({ type: 'apple', id: token });
      if (!user) {
        const isOauth = await userQueries.userIsOauth(token);
        if (isOauth) {
          return res.status(statusCodes.UNAUTHORIZED).send({
            data: {},
            code: responseCodes.INVALID_EMAIL,
            message: responseMessages.DIFFERENT_LOGIN_METHOD
          });
        }
        return res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.INVALID_EMAIL,
          message: responseMessages.USER_NOT_FOUND
        });
      }

      user = await userQueries.getUserByEmail(user.email);
      if (user.status != 'active') {
        return res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.USER_DELETED,
          message: responseMessages.USER_DELETED
        });
      }
      user.oauth = user.oauth ? true : false;
      user.notifications = user.notifications ? true : false;
      user.smsNotifications = user.smsNotifications ? true : false;

      const authToken = await generateJwtToken(user.ID, user.email);

      res.set('auth-token', authToken.toString());
      return res.status(statusCodes.OK).send({
        data: {
          user: _.pick(user, ['ID', 'email', 'firstName', 'lastName', 'profileIcon', 'oauth', 'notifications', 'phoneNumber', 'smsNotifications'])
        },
        code: responseCodes.OK,
        message: responseMessages.OK
      });
    } catch (e) {
      console.log(e);
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },
  appleSignup: async (req, res) => {
    try {
      let { firstName, lastName, email, profileIcon, timeZone, tzTimeZone, token } = res.locals.input;

      const type = 'apple';
      const existingOauth = await userQueries.getUserByOAuth({ type, id: token });
      if (existingOauth) {
        if (existingOauth.status != 'deleted') {
          return res.status(statusCodes.NOT_ACCEPTABLE).send({
            data: {},
            code: responseCodes.EMAIL_TAKEN,
            message: responseMessages.OAUTH_TAKEN
          });
        }
      }

      if (!email) {
        return res.status(statusCodes.OK).send({
          data: { "missingOAuthData": true },
          code: responseCodes.OK,
          message: responseMessages.OK
        });
      }
      let user = await userQueries.getUserByEmail(email);
      if (user) {
        //await userQueries.setOAuthData({ email, type, id: token });
        return res.status(statusCodes.NOT_ACCEPTABLE).send({
          data: {},
          code: responseCodes.EMAIL_TAKEN,
          message: responseMessages.OAUTH_TAKEN
        });
      }

      if (user) {
        if (user.status != 'deleted') {
          //await userQueries.setOAuthData({ email, type, id });
          return res.status(statusCodes.NOT_ACCEPTABLE).send({
            data: {},
            code: responseCodes.EMAIL_TAKEN,
            message: responseMessages.DIFFERENT_LOGIN_METHOD
          });
        }

        await userQueries.reactivateOAuthUser({ firstName, lastName, profileIcon, tzTimeZone, ID: user.ID });
        await userQueries.setOAuthData({ email, type, id });
      } else {
        await userQueries.createNewOAuthUser({ firstName, lastName, email, type, profileIcon, tzTimeZone, id: token });
      }

      user = await userQueries.getUserByEmail(email);
      user.oauth = user.oauth ? true : false;
      user.notifications = user.notifications ? true : false;
      user.smsNotifications = user.smsNotifications ? true : false;
      if (!user) {
        console.log('no user');
        return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
      }

      const authToken = await generateJwtToken(user.ID, user.email);

      res.set('auth-token', authToken.toString());
      res.status(statusCodes.OK).send({
        data: {
          user: _.pick(user, ['ID', 'email', 'firstName', 'lastName', 'profileIcon', 'oauth', 'notifications', 'phoneNumber', 'smsNotifications'])
        },
        code: responseCodes.OK,
        message: responseMessages.OK
      });
    } catch (e) {
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },
  authEmail: async (req, res) => {
    try {
      const { token, shouldRedirect } = res.locals.input;

      const decodedToken = await jwt.verify(token, process.env.JWT_SECRET);
      if (!decodedToken) {
        return res.redirect(`${process.env.APP_REDIRECT_URL}${10}`);
      }

      const { sub, data } = decodedToken;

      if (data.role) {
        if (data.role == "coach") {
          dashboardQueries.setCoachEmail(sub, data.email);
        } else if (data.role == "admin") {
          dashboardQueries.setAdminEmail(sub, data.email);
        }
      } else {
        const validToken = await userQueries.validateSecretToken(sub, data.secretToken);
        if (!validToken) {
          return res.redirect(`${process.env.APP_REDIRECT_URL}${10}`);
        }

        if (data.user && data.user.status == 'deleted') {
          console.log('reactivating user: ', data.user)
          await userQueries.reactivateUser(data.user);

          let isOauth = await userQueries.userIsOauth(data.user.ID);
          if (isOauth) {
            await userQueries.deleteOAuthData(data.user.ID);
          }
        }

        await userQueries.setUserSecretToken(sub, '');
        await userQueries.editUser({ ID: sub, email: data.email, status: 'active' });
      }

      let mergeVars = {
        header: "Email Confirmed!",
        paragraph: `Your email has successfully been set to ${data.email}`
      }

      await mailer.sendEMail(data.email, "Stake Something Email Confirmed", mergeVars);

      return res.redirect(shouldRedirect ? `${process.env.APP_REDIRECT_URL}${5}` : `${process.env.APP_REDIRECT_URL}${6}`);
    } catch (e) {
      console.log(e);
      return res.redirect(`${process.env.APP_REDIRECT_URL}${11}`);
    }
  },
  resendVerification: async (req, res) => {
    try {
      const { email } = res.locals.input;

      const user = await userQueries.getUserByEmail(email);
      if (!user) {
        return res.status(statusCodes.UNAUTHORIZED).send({
          data: {},
          code: responseCodes.INVALID_EMAIL,
          message: responseMessages.INVALID_EMAIL
        });
      }

      if (user.status != 'unconfirmed') {
        return res.status(statusCodes.BAD_REQUEST).send({
          data: {},
          code: responseCodes.EMAIL_TAKEN,
          message: responseMessages.EMAIL_CONFIRMED
        });
      }

      const secretToken = user.secretToken = uuid.v4().slice(24);
      await userQueries.setUserSecretToken(user.ID, secretToken);

      const token = await generateJwtToken(user.ID, { email: user.email, secretToken });

      let mergeVars = {
        header: "Almost done!",
        paragraph: `Hi ${user.firstName}, to finish signing up click the link below then open the app and tap 'Already have an account? Log in.'.`,
        link: `${process.env.URL_SCHEME}${process.env.HOST}/auth/token/${token}/?shouldRedirect=true`,
        link_text: "Confirm Email"
      }

      await mailer.sendEMail(user.email, "Stake Something Email Confirmation", mergeVars);

      return res.status(statusCodes.OK).send({
        data: {},
        code: responseCodes.OK,
        message: responseMessages.OK
      });
    } catch (e) {
      console.log(e);
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },
  forgotPassword: async (req, res) => {
    try {
      const { email, dashboard } = res.locals.input;

      const uid = uuid.v4().slice(24);
      let user;

      if (dashboard) {
        let userData = await dashboardQueries.getCoachOrAdminByEmail(email);
        if (userData && userData.role) {
          user = userData.user;
          user.email = user.emailAddress;
          user.firstName = user.fullName.split(" ")[0] || user.fullName
          if (userData.role == "admin") {
            await dashboardQueries.setAdminUID(user.ID, uid);
          } else if (userData.role == "coach") {
            await dashboardQueries.setCoachUID(user.ID, uid);
          }
        }
      } else {
        user = await userQueries.getUserByEmail(email);
        if (user && user.status != 'deleted') {
          await userQueries.setUserUID(user.ID, uid);
        }
      }

      if (user) {
        const link = `${process.env.FORGOT_PASSWORD_LINK}/${uid}`

        let mergeVars = {
          header: "Reset Password",
          paragraph: `Hi ${user.firstName}, a password reset has been requested for your account. Click the link below to change your password. If you did not request the reset, ignore this email.`,
          link: link,
          link_text: "Change Password"
        }

        await mailer.sendEMail(user.email, "Stake Something Password Reset Request", mergeVars);
      }

      res.status(statusCodes.OK).send({
        data: {},
        code: responseCodes.OK,
        message: responseMessages.OK
      });
    } catch (e) {
      console.log(e);
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },
  changePassword: async (req, res) => {
    try {
      const { UID, password, set } = res.locals.input;

      let user = await userQueries.getUserByUID(UID);
      if (user) {
        const hashedPassword = await generatePasswordHash(password);
        await userQueries.editUser({ ID: user.userID, password: hashedPassword });
        await userQueries.setUserUID(user.userID, '');

        if (!set) {
          let mergeVars = {
            header: set ? "Password set" : "Password reset",
            paragraph: set ? "Your password has been set successfully!" : `Your password has been reset successfully!`,
          }

          const emailUser = await userQueries.getUserByID(user.userID);
          await mailer.sendEMail(emailUser.email, set ? "Stake Something Password Set" : "Stake Something Password Reset", mergeVars);
        }
      } else {
        let userData = await dashboardQueries.getCoachOrAdminByUID(UID);
        if (userData && userData.role) {
          const hashedPassword = await generatePasswordHash(password);

          if (userData.role == "admin") {
            await dashboardQueries.setAdminPassword(userData.user.ID, hashedPassword);
          } else if (userData.role == "coach") {
            await dashboardQueries.setCoachPassword(userData.user.ID, hashedPassword);
          }
        } else {
          return res.status(statusCodes.UNAUTHORIZED).send({
            data: {},
            code: responseCodes.INVALID_TOKEN,
            message: responseMessages.INVALID_TOKEN
          });
        }
      }

      res.status(statusCodes.OK).send({
        data: {
          isUser: user ? true : false
        },
        code: responseCodes.OK,
        message: responseMessages.OK
      });
    } catch (e) {
      console.log(e);
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },
  getChangePasswordLink: async (req, res) => {
    try {
      const ID = req.user;
      const userData = await dashboardQueries.getCoachOrAdminByID(ID);
      if (userData && userData.role) {
        const uid = uuid.v4().slice(24);
        if (userData.role == "admin") {
          await dashboardQueries.setAdminUID(userData.user.ID, uid);
        } else if (userData.role == "coach") {
          await dashboardQueries.setCoachUID(userData.user.ID, uid);
        }

        return res.status(statusCodes.OK).send({
          data: { uid },
          code: responseCodes.OK,
          message: responseMessages.OK
        });
      }

      return res.status(statusCodes.UNAUTHORIZED).send({
        data: {},
        code: responseCodes.INVALID_TOKEN,
        message: responseMessages.INVALID_TOKEN
      });
    } catch (e) {
      return res.status(statusCodes.SERVER_ERROR).send(serverErrorResponse());
    }
  },

export { logIn, signUp, forgotPassword, forgotPasswordForm, passwordRecovery, resetPassword, appleLogin, googleLogin, facebookLogin };
