const paths = {};
const definitions = {};
const responses = {};

export function generateDocumentation() {
    return {
        _swagger: '2.0',
        get swagger() {
            return this['_swagger'];
        },
        set swagger(value) {
            this['_swagger'] = value;
        },
        schemes: ['http'],
        info: {
            contact: {
                name: 'Cinnamon agency',
                email: ''
            }
        },
        host: `localhost:3000`,
        basePath: '/api',
        paths,
        definitions,
        responses,
        parameters: {}
    };
}
