import bcrypt from 'bcrypt';

const comparePassword = async (password: string, userPassword: string) => {
    const match = await bcrypt.compare(password, userPassword);
    return match;
};

export default comparePassword;
