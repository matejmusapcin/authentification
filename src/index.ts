import express, { Request, Response, NextFunction } from 'express';
import * as dotenv from 'dotenv';
import cors from 'cors';

import routes from './routes';

dotenv.config();

const PORT = process.env.PORT || 3000;
const app = express();

app.use(
    cors({
        credentials: true,
        origin: true
    })
);
app.use(express.json());
app.use(
    express.urlencoded({
        extended: true
    })
);

app.use('/api', routes);

// error handler middleware
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
    const statusCode = err.statusCode || 500;
    console.error(err.message, err.stack);
    res.status(statusCode).json({
        code: statusCode,
        message: err.message
    });
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}!`);
});
